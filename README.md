
# SHSPE - SHow SPEctra


**root macro to SHow SPEctra**


## 1. Installation of root

*Including file (impossible in markdown?):*


[README_rootinst.md](README_rootinst.md)











## 6. shspe compilation



  1. COMPILATION of **shspe**:

  ```
  cmake .
  cmake --build .
  ```
  shspe  .so library should be installed in root macro directory (verify it)


  2. **Additionally:**

  you may want to install the *~/.rootrc* and *~/.rootlogon*
  ```sh
  # if you install root for the very first time:
  cd 	init_scripts
  ./install_initscripts
  ```


## 6.1. run root and shspe()

```
# why not
rootn.exe -l
```

### 6.1.1  autoload_libs.C

If a file `autoload_libs.C` is found in current directory,
it is processed during `root` start, if the above `.rootlogon` is installed
```
{
// possible content of autoload_libs.C
// starting some processes automatically
gROOT->ProcessLine("shspe()");
}
```
if no such file, type
```cpp
root [0] shspe()
```


### 6.1.2. `.REMOTE_DATA_DIR` file

`shspe` sees only the current directory. However, it is possible to add one more directory with DATA. Create a *hidden* file:`.REMOTE_DATA_DIR` that contains the /PATH/TO/DATA.

`shspe` - on *Openfile* - will also read `.root` files from there.


### 6.2. OPEN FILE


 * Click *open file*
 * Root files that are on ```.REMOTE_DATA_DIR``` path are displayed with a tilde `~` sign.
 * Click *some root file with histograms*
 * The filename is written to *hidden* `.CURRENTFILE` file.
   - This could be usefull if you needed to deduce the run number in some script or something.
   - But this can also create difficulties when two shspe() sessions are open in the same directory.




###  6.2.1 OPEN FILE tricks

The  shspe() can also open **(not supported now because of mongodb?)**:

* mysql reference files with a name like `tablename.mysql` /host-user-password/ that refers to mysql database named `test`. The database fields `a` to `h` can be viewed as `TGraph`.

* when running `rootn.exe` `mmap.histo` file, that shares memory with histograms from digitizer acquisistion is opened and histograms are read.



![in construction](https://upload.wikimedia.org/wikipedia/en/4/4f/Under_construction.JPG)



### 6.3.  FILE SAVE

You can save
* canvases + histograms on screen click ```SaveCanvas``` - you can input you filename into the text field and save multiple format, with a time tag. Withou a name it stores canvas as 1.root, 2.root,... 9.root that can be recovered later with ```LoadCanvas```
* all histograms click ```SaveAllSpectra``` - filename is always based on datetime
* copy histogram from file to memory ```Spectrum2Memory``` and go to memory later and compare histograms from different files there.



4.2 Fitting
------------

**4.2.1 Prepare fit**

**4.2.2 Fit procedure**

**4.2.3 Save fit**

Several files are created ```zfitresults``` the extensions are

*```.eff``` - for calibration of efficiency
*```.ecal``` - just energy and channel
*```.tmp``` - same as eff but only the last fit - can be parsed by script
*```final.root``` - all objects of results
*```tmp.root``` - as root, but the last fit


5.1 Other functions
------------

There are more functions loaded with shspe:



**gr_  group**

functions to play with graphs
```
grhelp()
```

**joingraphs**

join several graphs to one multigraph

**MPad  group**

MPadGetByName()
MPadCreate()
MPadPrintIn()

for use with TCounter



**TCounter  group**

not interesting now



**cuts  group**

uses **cuts.root** file for storing the cuts
```
cutload()
cutsave()
cutls()
cutrm()
cutcp()
```

### 9.1 some specific  details

creates .CURRENTFILE on open

checks .REMOTE_DATA_DIR on open

searches for a script  *shspe.pk_mysql* setup file on savefit

------------------------------


https://stackoverflow.com/questions/36183071/how-can-i-preview-markdown-in-emacs-in-real-time
