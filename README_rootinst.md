ROOT installation
=====================

> *a remark:  since resulting root compilation depends on python binary,
> we test the root compilation in python environment*
```
mkvirtualenv root
workon root
# config and compilation goes here;
#the option -DPYTHON_EXECUTABLE=$HOME/.virtualenvs/root/bin/python3
```

*remark 2:*
```
It is now compilable under downloaded precompiled root:

root_v6.22.06.Linux-ubuntu20-x86_64-gcc9.3.tar.gz

ROOT Version: 6.22/06
Built for linuxx8664gcc on Nov 27 2020, 15:14:08
From tags/v6-22-06@v6-22-06
```

# Pro VERSION v6-18-04

 - check root.cern.ch website https://root.cern.ch/build-prerequisites
 - python3 is very useful, `pyroot` and `roopy` should work at the end
 - to have a clean python installation, I use python virtual environments,
   which makes  certain complications. `root` environment or `jupyter` environment
   are used.

## 0. Python environmets.

 -  BE INSIDE ONE

## 1. Prerequisites

 Ubuntu 20.04, root 6-22-02

```
sudo aptitude install libx11-dev  gcc g++ libpcre3-dev xlibmesa-glu-dev libglew1.5-dev libftgl-dev libcfitsio-dev libgsl0-dev

```

### UBUNTU 20.04
crashing with openssl built 11%, openssl-dev not installable on fossa
```
sudo apt install librust-openssl-dev
```
crashed without?




## 2. Clone from  GIT and checkout Pro version


 - **follow**  https://root.cern.ch/building-root
```
 git clone https://github.com/root-project/root.git
 cd root
 git tag
 git checkout -b v6-18-04 v6-18-04
 # new pro ... testing
# git checkout -b v6-20-00 v6-20-00
# git checkout -b v6-20-04 v6-20-04
# ubuntu 20.04 OCT20
# git checkout -b v6-22-02 v6-22-02
 git checkout -b v6-22-02 v6-22-02
 cd ..
```


## 3.  Make relevant local directories

```
  mkdir ~/root
  mkdir ~/root_macros
  mkdir ../root.build
#supposing you are in root/
  cd ../root.build
```

## 4. Configure

 - to have **pyroot** it is needed to give a correct python executable. Normally '/usr/bin/python/' is used, but with virtual environments, better to use `which python3`.

 ```
  cmake ../root -Dfftw3=OFF -Dmathmore=OFF  -Dpython3="ON"   -Dminuit2=ON  -Dbuiltin-freetype=ON   -Dmysql=ON  -Droofit=ON  -DPYTHON_EXECUTABLE=`which python3`  -DCMAKE_INSTALL_PREFIX=$HOME/root
 ```


## 5. Compile and install locally


```
#time cmake --build . -- -j4
#cmake --build . --target install
#
# NOW 202101 - this is enough:
time cmake --build . -- install -j4
```

 worked without problems now.

## 6. Tests

- basic test, remember to run `thisroot.sh` before testing.

```
# in virt environment - if exists:
ipython3
import ROOT
import root_pandas
import root_numpy
quit()
```

- python3 test from tutorials:

```
cd $HOME/root/tutorials/pyroot
python3 benchmarks.py # this is ok
#
#python3 demo.py     # this crashes as it needs python2
#
```

- BEST PYTHON LIBRARIES
   - `pyroot` - classics
   - `roopy` - ? manipulates trees, histos, plot with matplotlib ?
   - `root-numpy` - to fill the histograms https://pypi.org/project/root-numpy/
   - uproot -  read/write files. (uproot4)



- shspe compilation

```
 cmake . # you can leave it to create all .rc files...
 cmake --build .
```
Shspe is copied to some PATH:

*PATH:    /home/ojr/root_macros               ..  YES, I copy here*

try `root -e 'shspe()'` to directly start shspe, or create the file
`autoload_libs.C` with
```
{
shspe();
}
```

that will be searched and proceeded every root start - if you allowed
during shspe`cmake .`.


# =============== END OF THE NEW MANUAL ===================================

 *what is after this line comes from earlier versions *

# =============== END OF THE NEW MANUAL ===================================

 *what is after this line comes from earlier versions *

# =============== END OF THE NEW MANUAL ===================================

 *what is after this line comes from earlier versions *

1. Prerequisites:
-------------------

basically, better check root.cern.ch website https://root.cern.ch/build-prerequisites


```
aptitude install dpkg-cross
aptitude install libxext-dev
aptitude install libxpm-dev
aptitude install libxft-dev
```


2. clone from  GIT
--------------

**follow** https://root.cern.ch/content/release-61006 **and**
https://root.cern.ch/building-root


The release specific tag can be obtained using:
```
git clone http://github.com/root-project/root.git`

cd root
git checkout -b v6-10-06 v6-10-06
```

Later ...

```
  git clone https://github.com/root-project/root.git
  cd root
  git tag
git checkout -b v6-12-04 v6-12-04
cd ..
```

python3 FLAG didnt work here *-DPYTHON_EXECUTABLE=/path/to/desired/python* may help  ... Later ...

```
  git clone https://github.com/root-project/root.git
  cd root
  git tag
git checkout -b v6-14-06 v6-14-06
cd ..
```

**6-16-00** is a recommended Pro

```
  git clone https://github.com/root-project/root.git
  cd root
  git tag
git checkout -b v6-16-00 v6-16-00
cd ..
```

If there are problems (after `checkout master` and `pull`), dont forget to get `git pull origin master --tags`



Version 6-16-00: one ok, one problem
```
  git clone https://github.com/root-project/root.git
  cd root
  git tag
git checkout -b v6-16-00 v6-16-00
cd ..
# crash in installation VDT : trying
# cmake ... -Dtmva=OFF ... -Dbuiltin_xrootd=ON
# cmake --build . --target VDT
# cmake --build .
# cmake --build . --target install  # WORKED finaly with pyroot
```





Version 6-18-00 with Ubuntu 1804:  it seems the same problem with VDT as in 6.16.
Apt instalation of `libssl-dev` needed for XROOTD.
There was another problem with "Version mismatch between Python interpreter and libraries
difference of python libs and python."
- Removal of `FATAL_ERROR` and putting `STATUS`
at  `../root/cmake/modules/SearchInstalledSoftware.cmake` have helped.
SLOW notebook (7:20h on 1 core) was making BIG problems, I tried several things and
it finally installed:

```
-Dvdt=ON # added in config
# added line in SearchInstalledSoftware: set_property() like this
  set(builtin_vdt ON CACHE BOOL "Enabled because external vdt not found (${vdt_description})" FORCE)
	set_property(GLOBAL APPEND PROPERTY ROOT_BUILTIN_TARGETS VDT)
# additional installs
sudo aptitude install libx11-dev  gcc g++ libpcre3-dev xlibmesa-glu-dev libglew1.5-dev libftgl-dev libcfitsio-dev libgsl0-dev
```

```
  git clone https://github.com/root-project/root.git
  cd root
  git tag
git checkout -b v6-18-00 v6-18-00
cd ..
# crash in installation VDT : trying
# cmake ... -Dtmva=OFF ... -Dbuiltin_xrootd=ON
# cmake --build . --target VDT
# cmake --build .
# cmake --build . --target install  # WORKED finaly with pyroot
```




3. Prepare directories and compile:
--------------------
```sh
  mkdir ~/root; mkdir ~/root_macros
  #considering you are in ../root/
  cd ../
  mkdir root.build
  cd root.build

  # if python virtual env.: change the -DPYTHON_EXECUTABLE
  #
  cmake ../root -Dfftw3=OFF -Dmathmore=OFF  -Dpython3="ON"   -Dminuit2=ON  -Dbuiltin-freetype=ON   -Dmysql=ON  -Droofit=ON  -DPYTHON_EXECUTABLE=/usr/bin/python3  -DCMAKE_INSTALL_PREFIX=$HOME/root
  #
  # compile, measure time, use maybe 8 cores
  # in a case of crash, go with 1 core
  #
  # funny trick
export CORES=`cat /proc/cpuinfo | grep processor | wc -l`
echo $CORES
 time  cmake --build . -- -j$CORES
 #
 # install now to empty ~/root/
 #
 cmake --build . --target install
```


|  CPU /cores             |  ver.         | tot   | single |
|------------------------ |---------------|-------|--------|
| i7-6500U @ 2.50GHz j4   | root 6.14.06  | 1:19  | 5:16   |
| i7-6500U @ 2.50GHz j1   | root 6.14.06  | 2:38  | 2:38  |
| i7-8700  @ 3.20GHz j6   |  v6-14-06     | 0:20  | |
| i7-8700  @ 3.20GHz j12  | v6-14-06      | 0:18  | |
| i5-3317U @ 1.7GHz  j4   | v6-14-04      | 1:41  | |
| i5-3317U @ 1.7GHz  j4   | v6-12-06      | 1:41  | |
| i5-3317U @ 1.7GHz  j4   | v6-16-00      | 1:57  | |


4. Afterparty
--------------------------

```
source $HOME/root/bin/thisroot.sh
```

- PUT this .zshrc or .bashrc or both
` source $HOME/root/bin/thisroot.sh`

### Python !
- maybe - when python is upgraded on PC, new compilation of root is necessary.... (due to PYTHON_EXECUTABLE flag)

### Tests:
- ipython3
```python
import ROOT
# if no crash, it is there
```

- python tutorials/libraries
```sh
cd $HOME/root/tutorials/pyroot
python3 demo.py     # this crashes as it needs python2
python3 benchmarks.py # this is ok
#
# there could be games with PATH
#
# export PYTHONPATH=$PYTHONPATH:$HOME/opencv.build/python_loader
# export PYTHONPATH=$HOME/root/lib
#
```

- compile **shspe**:
```
```

- test if **mmap** feature is included:
```sh
rootn.exe
# open mmap.histo from/by shspe
```

- all different problems with version v6-10 and less are discussed  in older commits from 2018





5. Various tricks
------------------------
#### mime type - automatic open in browser / xdg-open

Mostly unsuccessfull, look at
`https://askubuntu.com/questions/525953/use-custom-command-to-open-files`
and this `root.desktop` in `/usr/share/applications` could help
```
[Desktop Entry]
Name=ROOT
GenericName=root
TryExec=/home/ojr/root/bin/root
Exec=/home/ojr/root/bin/root -e "{shspe();}" %U
Terminal=true
Type=Application
Categories=GTK;Utility;TerminalEmulator;System;
```

and update with `sudo xdg-mime default root.desktop application/octet-stream`
