#!/usr/bin/env python3

# test minuit
# https://iminuit.readthedocs.io/en/stable/tutorial/cost_functions.html
#
#  NLL slightly better than LS to find minimum BUT
#  if LS doesnt find, still has better BG
#  than NLL if lost => background is biased for NLL
#
#  if both loose:  NLL Chi2/d = 50;   LS = 12
#
#  NLL needs cfd for binned... but returns Areas
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


import numpy as np
from matplotlib import pyplot as plt
import math

# --- range of the histogram
xrange = 0, 300
fwhm = 3

# ------ generate the events
rng = np.random.default_rng()  # create a  new generator class (seed 1)
xdata = rng.normal(100, fwhm/2.35, size=3500)
xdata = np.append(xdata, rng.normal(200, fwhm/2.35, size=2500))

xdata = np.append(xdata, rng.uniform( *xrange, size=10000)) # *= unziped



# ------- create a histogram from the events
yval, xe = np.histogram(xdata, bins=xrange[1]-xrange[0], range=xrange ) # n=y; xe=bins
cx = 0.5 * (xe[1:] + xe[:-1]) # middle of the bins 0.5 - 299.5
# print(cx)
dx = np.diff(xe) # ? bin width?
print(f"i... bin (min/max)width  [{min(dx)}...{max(dx)}]; bins={len(dx)}")



# ----- plot histogram as points
plt.errorbar(cx, yval, yval ** 0.5, fmt=".k", alpha=0.3) # point and blacK
# plt.plot(xdata, np.zeros_like(xdata), "|", alpha=0.02); # show data volume
#plt.show()


# ------------------------- MINUIT PART ----------------
from iminuit import cost, Minuit
import iminuit
from numba_stats import norm, uniform # faster replacements for scipy.stats functions

# -------------------- model must be cumulative density function (for binned)
#  xe and xrange ARE GLOBAL
def model_density_cdf(xe,   n0,n1, nbkg, mu0, mu1, fwhm):
    return (n0 * norm.cdf(xe, mu0, fwhm/2.35) +
            n1 * norm.cdf(xe, mu1, fwhm/2.35) +
            nbkg * uniform.cdf(xe, xrange[0], xrange[1] - xrange[0]))


c = cost.ExtendedBinnedNLL(yval, xe, model_density_cdf)
m = Minuit(c,
           n0=1100, n1=900, nbkg=4900,
           mu0=115, mu1=205, fwhm=5)

m.limits["n0","n1", "nbkg", "fwhm"] = (0, None)
m.limits["mu0"] = (xrange[0], xrange[1])
m.limits["mu1"] = (xrange[0], xrange[1])

m.migrad()
print(m.fmin)   #NICE table
print(m.params) # NICE table

#print("minuit:")
#print(m)
xm = np.linspace(xe[0], xe[-1])

plt.stairs(np.diff(model_density_cdf(xe, *[p.value for p in m.init_params])), xe,  ls=":", label="init")

plt.stairs(np.diff(model_density_cdf(xe, *m.values)), xe, color='r',label="fit")


# ------ bin values are plotted, not smooth function
#plt.plot(xe[:-1], np.diff(model_density_cdf(xe, *m.values)),'c', label="fit-")
#plt.plot(xe[1:], np.diff(model_density_cdf(xe, *m.values)) ,'y',label="fit+")
#plt.plot(xe[1:]-0.5, np.diff(model_density_cdf(xe, *m.values)),'r',label="fit")

plt.legend()
#plt.show()


# --------------chi2
def model_chi2(xe,   n0,n1, nbkg, mu0, mu1, fwhm):
    # print(xe)
    f1 = n0/fwhm*3 * np.exp( -1*(xe-mu0)**2/2/(fwhm/2.35)**2  )

    f2 = n1/fwhm*3 * np.exp( -1*(mu1-xe)**2/2/(fwhm/2.35)**2  )

    f3 = np.zeros_like(f1)+nbkg/(xrange[1] - xrange[0])

#    return (n0 * norm.cdf(xe, mu0, fwhm/2.35) +
#            n1 * norm.cdf(xe, mu1, fwhm/2.35) +
#            nbkg * uniform.cdf(xe, xrange[0], xrange[1] - xrange[0]))

    #print(f1)
    return (f1+f2+f3)


#print(len(xe), xe)
#print(len(yval))
# --- middle of interval = cx
c2 = cost.LeastSquares(cx, yval, yval**0.5, model_chi2)
#m = Minuit(c)
print("=====================least squares====")
m2 = Minuit(c2,
           n0=350, n1=300, nbkg=5000,
            mu0=115, mu1=205, fwhm=5)
m2.limits["n0","n1", "nbkg", "fwhm"] = (1, None)
m2.limits["mu0"] = (xrange[0], xrange[1])
m2.limits["mu1"] = (xrange[0], xrange[1])
m2.migrad()
#print(m2.errors) # error view
#print(m2.values) # value view


# print(iminuit.util.FMin() ) # ????
#print(m2.covariance) # NICE but NOT NEEDED matrix
print(m2.fmin)   #NICE table
print(m2.params) # NICE table

print(m2.values)
print(m2.values[0])
print(m2.values['n1'])

#print(m2.accurate) # true
#print(m2.nfit) # nparams
#print(m2.fval) # fval = FCN

plt.errorbar(cx, yval, yval ** 0.5, fmt=".k", alpha=0.1) # point and blacK

plt.stairs(model_chi2(cx, *[p.value for p in m2.init_params]), xe,  ls=":", label="init2",color="y")

plt.stairs(model_chi2(cx, *m2.values), xe, color='m',label="chi2")

plt.legend()
plt.show()
