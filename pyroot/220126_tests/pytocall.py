#!/usr/bin/env python3

import ROOT

print("I just entered pytocall.py file")

def mypy():
    print(f"Here I and in python function {4/3} and OK.")
    return 3.1415926

# ----- return some ROOT object ------
def mypyth1f():
    print(f"Here I and in python function {4/3} and OK - BUT TH1F return.")
    h = ROOT.TH1F("h1","haha",100,0,100)
    return h

# ----- return some ROOT object ------
def mypyfill(h):
    print(f"FILL")
    h.fill(100)
    return h


# class MyPyClass:
#     def __init__(self):
#         print("CLASS i am in __init__ of MyPyClass")
#     def say(self):
#         print("CLASS i am saying inside  MyPyClass !!!!!!")
#         return 1

# a = MyPyClass()
# print("kuku")
# a.say()


# ------ here i want to try to run something rootbased
h = ROOT.TH1F("h","ha",1000,0,1000)
h.Draw()


# class MyMainFrame( ROOT.TGMainFrame ):
#     def __init__(self):
#         #ROOT.TGMainFrame.__init__( self, None, 400,400 )

#         print("I am in constructor")
#         self.hBframe = ROOT.TGHorizontalFrame(self)
#         self.fListBox = ROOT.TGListBox(self, 100)
#         self.fSelected = ROOT.TList
#         self.fListBox.AddEntry("a", 1)

#         print("hb af")
#         self.hBframe.AddFrame(self.fListBox ,  ROOT.TGLayoutHints(ROOT.kLHintsLeft |  ROOT.kLHintsExpandY, 0, 0, 0, 0))
#         print("af 1", self.hBframe)
#         self.AddFrame(self.hBframe, ROOT.TGLayoutHints( ROOT.kLHintsExpandX |  ROOT.kLHintsExpandY , 5, 5, 0, 5))
#         print("setname")
#         ROOT.SetWindowName("List Box");

#     def NDim( self ):
#         return 1



# mmframe = MyMainFrame()

print("... out the pytocall file")
