
# ------------------------- MINUIT PART ----------------
#  pip3 install iminout  numba_stats numpy
from iminuit import cost, Minuit
import iminuit
from numba_stats import norm, uniform # faster replacements for scipy.stats functions
import numpy as np

#
# I need to go to chebyshev
#


def main(x,y,dy):
    print("__________________________________________________")
    global bin1 # trick for better convergence
    bin1 = x[0]

    # --++++++++++++++++++++++++++++------------chi2
    def model_chi2(x,   a,b,c):
        global bin1
        #f = a* x**2 + b*x + c
        f = np.polynomial.Chebyshev( [a,b,c] )(x-bin1)
        return f


    # ---- for histograms, use cx...
    print(".............iminuit.............>")
    c2 = cost.LeastSquares(x, y, dy, model_chi2)

    m2 = Minuit(c2,
                a=1, b=1, c=1 )
#                a=1.581 , b=-23536.7, c=8.75959e+7 + 3140 )

    print( "INIT PAR ",sum(y)/len(y) )
    # m2.limits["a", "b", "c"] = (0, None)

    #    m2.limits = (-10000,10000)
    #    m2.scan(ncall=500000)
    #    print(m2.fmin)   #NICE table
    #    m2.simplex()       # DO MINIMIZATION <<<<<<<<<<
    #    print(m2.fmin)   #NICE table


    m2.migrad()       # DO MINIMIZATION <<<<<<<<<<


    #print(m2.errors) # error view
    #print(m2.values) # value view

    print(m2.fmin)   #NICE table
    print(m2.params) # NICE table

    yf = model_chi2( x,
                     m2.values['a'],
                     m2.values['b'],
                     m2.values['c'] )


    #print(yf)
    print(f"i... Chebyshev parameters are not real! they are for shifted X")
    print("_________________________________________________")
    return yf
