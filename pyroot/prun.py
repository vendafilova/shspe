import ROOT
import glob
import os

import importlib # dynamic import
import sys # to unimport

#
#  THIS FILE CANNOT BE EDITED without exiting ROOT everytime
#
#---------------------------------------- functions called from pr_*.py


def register(obj, NAME ):
    """
    For plotting on current canvas or access from CLING,
 each object has to be registered
    """
    #NAME = "fit"
    obj.SetName(NAME)
    # obj.SetTitle(NAME)
    oldg = ROOT.gROOT.GetListOfSpecials().FindObject(NAME)
    if oldg:
        ROOT.gROOT.GetListOfSpecials().Remove(oldg)
    ROOT.gROOT.GetListOfSpecials().Add(obj)
    return



#========================================== functions called from C++


def import_pr_input():
    """
    get the input from TText object and  eventual arguments
    """
    # 1/ get input
    lin = ROOT.gROOT.GetListOfSpecials().FindObject("pr_input")
    # print(f"P... infput function found - see: {lin}")

    # for a case if called from python code
    if ROOT.addressof(lin)==0:
        print("X... no TText present ... called from python?")
        return None,None

    inname = lin.GetName()
    intit = lin.GetTitle()

    # print(f"P...    title={intit},  name={inname} " )
    params = []
    if intit.find(" ")>0: # space means parameters follow
        params = intit.split()
        intit = params[0]
        params.pop(0)
        print("i... ARGUMENTS:", params)

    return intit,params # ModName, list


def listpy():
    """
    return all pr_*.py files (in the current directory)
    """
    #print("P... in fun listpy")
    # --- I list all files  pr_ py:


    files = glob.glob("pr_*.py")
    if len(files)==0:
        print("X... NO python FILES available")
        return

    files = [x for x in files if x.find("pr_model_")!=0]

    print(f"i... modules available: \n{files}\n")
    return files




#================================================================== MAIN PART

def loadpy( *args ):
    """
    import module - based on the TText object Title created in C++ earlier
    """
    #print("P... in fun listpy")


    intit, params = import_pr_input()
    #print(f" intit={intit}, params={params}")

    if intit is None:
        if len(args)!=2:
            print("X... I think I am called from python, I need two arguments")
            return
        intit = args[0]
        params = args[1].split()
        print(f"fi... called from python, looking for module {intit}; params = {params}")


    files = listpy()


    for ffile in files:
        construct =  ffile.lstrip("pr_")
        # print(construct)
        construct =  construct.rstrip("py")[:-1] # prioblem w .py
        # print(construct)
        print(f" ...    searching  /{intit}/ in /{ffile}/ <={construct}")
        if intit== construct:
            #print(f"P... got {ffile}")

            #print("i... trying to importlib:", item.GetTitle() )

            # UNimport module first - case there was an error there previously
            try:
                sys.modules.pop( f"pr_{construct}")
            except:
                print("")


            # IMPORT
            module = importlib.import_module( f"pr_{construct}" )
            # AND run the modules' main()
            if len(params)>0:
                res = module.main( *params)
            else:
                res = module.main()



            # print("i... leaving prun.py")
            return res # I cant do better with adding to Specials and gDirectory
        #======================================================================== END

            # UNimport module - NOT HERE ?! - but no impact on gDirectory
            # sys.modules.pop( f"pr_{construct}")


            # ----- this way I get things back to C++-----------NO BETTER THAN in the module itself
            print("----------------- in prun.py")
            # item = ROOT.TText( 0.01,0.01, "This is an output from prun.py" ) # Title == it
            # item = ROOT.TGraphErrors() #
            # item.SetName("pr_output")
            # item.SetTitle("Some output title text")

            los = ROOT.gROOT.GetListOfSpecials() # here - no problem to reach it
            for n in range( los.GetSize() ):
                objec = los.At(n)
                typeob = objec.ClassName()
                if (typeob=="TGraphErrors")or(typeob=="TGraphErrors")or(typeob=="TH1F"):
                    #print( "adding to gDirectory", type(objec) , objec ) # IT DOESNOT WORK
                    #ROOT.gDirectory.Add( objec ) # doesnt work really
                    los.Add(objec)                # this works
            #los.ls()
            #ROOT.gDirectory.Add(item)
            print("---------gDir list:")
            ROOT.gDirectory.ls()

            #item.GetTitle().rstrip(".py") )


            return

    print(f"X... no file like pr_{intit}.py found")
    return
