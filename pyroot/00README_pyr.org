* Instructions

** Introduction

 Root has a great commandline interpreter CLING (earlier CINT). However,
C++ lacks the simplicity of Python. Fortunately, there exists PyRoot.

 This is an attempt to deliver a framework, that :
 - enables to call Python code from the CLING of root
 - load and fit data (Graphs at the moment)
   - with MINUIT, but from Python (iminuit), where it is much more simple
   - easily construct and call new models (one file per model) without a fuss

** Example in bash

 Load and show or fit (with load) and show.

#+BEGIN_EXAMPLE
# load only
./pr_load.py tracked.dat x,y

# OR load and fit

./pr_fit.py tracked.dat pol1 n,x,y
#+END_EXAMPLE



/tracked.dat/ file may (but dont need to) have a header, hash at col 1 is always
 considered as a comment.  /#COLNAME/ defines the names on axes.

 /#LOAD_AS/ is the information for TGraph - which column is /x/ or /y/ or /dx/ and /dy/,
if any - in the order.
/x,y/ in this example will be changed automaticaly to /x,y,col1/ to match the number of
columns.

#+BEGIN_EXAMPLE
#COLNAME: frame,x,y
#LOAD_AS: x,y
1 2 3
#+END_EXAMPLE


** Example in root

The objects should remain in memory, so one can refer them.
And it is possible to go more like step by step.

#+BEGIN_EXAMPLE
# load only - this calls /pr_load.py/.  /n,y,x/ overrides the #LOAD_AS: in the dat file
.x prun.C("load tracked.dat n,y,x")

# object is in memory (should be), Type
tracked
# as a response for existing object, something confirming should appear

# this calls pr_fit.py on the existing object /tracked/
.x prun.C("fit tracked pol1")


#+END_EXAMPLE


** Model construction

See e.g. /pr_model_pol1.py/

*** Basics
This file is simply imported (and the /main/ is called) from the parent /pr_fit.py/,
when you ask /fit pol1/.

When calling /fit whatever/ - /pr_model_whatever.py/ is tried to import.

*** Function to minimize
The function defined inside -  /model_chi2/
 - takes /x/ as the x-coordinate,
 - all other argument are parameters one by one

/x/ can be also a numpy array (which makes it fast). So any operation on /x/ must
be defined numpy-friendly.

Example:  exponential function:  /np.exp( -1*(x-mu0)**2/2/(fwhm/2.35)**2  )/

*** Initial values and returning values
Concerning the lines starting with c2 = cost.LeastSqueares():
 - Minuit() needs some initial values for parameters /a/, /b/
 - /migrad/ runs the minimization
 - /yf/ - output is constructed based on the final values, that are stored in /m2.values['a']/ etc...
 - /yf/ is sent back to parent


** Next
 We should prepare some persistent output of the fit + figure,
test more complex cases, use Chebyshev polynomials etc...

/Tested a bit on ROOT Version: 6.22/08/

* Appendix
** What is available to do in CLING

  - I can prompt to python, but not good
  - CALLBACK - python function that can be used/called in root
    - create python class Linear:  def __call__(self,x,par)
    - TF1(... Linear()...)
  - INHERIT from C++ classes - using an adaptor class with public: word
  - JIT reflection
    - have A.h, ROOT.gINterpretter.ProcessLine('#include A.h")
    - call classes of functions defined in A.h

** What I have NOW
*** 2022 01 26

  1. I have =prun.C= and =prun.py=.
  2. I can compile ~.L prun.C+~ in root and use ~prun("fitpol")~
  3. This calls a python macro =pr_fitpol.py= if it exists
  4. All parameter transfers btwn C-Py use gROOT...specials: TText Name&Title

 Benefits:
  + It is possible to tune the python code from commandline
  + The created objects persist when in CLING

 To solve:
  - calling function with parameters
    - maybe calling python from the python!
      - then, all the eventual parameters can stay in the same string



 #+BEGIN_SRC C

 .x prun.C("fitpol")

 #+END_SRC

*** 2022 02 04
  1. prun [C|py]
  2. load, fit, help
  3. calling from root + un-import modules to allow edit after error
  4. several polynomial models - pr_model*
  5. can be called from bash too
     - because fit eventualy can run pr_load.main() to read data
  6. HEADER structure for dat file
** Web resources
 - https://root.cern/manual/python/#tpython-running-python-from-c
 - https://root.cern/doc/master/classTPyReturn.html
 - https://root-forum.cern.ch/t/filling-python-list-in-c-class/18579
 - https://gitlab.ikp.uni-koeln.de/jmayer/hdtv/-/blob/2c62b18aa612079f51f53cfccd8b53ee5c251637/hdtv/fitpanel.py
 - https://gitlab.ikp.uni-koeln.de/jmayer/radiation
 - https://gitlab.ikp.uni-koeln.de/jmayer/radiation
 - https://iminuit.readthedocs.io/en/stable/tutorial/simultaneous_fits.html
 -
