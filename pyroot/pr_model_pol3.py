
# ------------------------- MINUIT PART ----------------
#  pip3 install iminout  numba_stats numpy
from iminuit import cost, Minuit
import iminuit
from numba_stats import norm, uniform # faster replacements for scipy.stats functions
import numpy as np


#
# I need to go to chebyshev
#


def main(x,y,dy):
    print("__________________________________________________")
    global bin1 # trick for better convergence
    bin1 = x[0]


    # --++++++++++++++++++++++++++++------------chi2
    def model_chi2(x,   a,b,c,d):
        #f = a* x**3 + b*x**2 + c*x + d
        f = np.polynomial.chebyshev.Chebyshev( [a,b,c,d] )(x-bin1)
        return f


    # ---- for histograms, use cx...
    print(".............iminuit.............>")
    c2 = cost.LeastSquares(x, y, dy, model_chi2)

    m2 = Minuit(c2,
                a=sum(y)/len(y), b=1, c=1, d =1 )

    # m2.limits["a", "b", "c"] = (0, None)

    m2.migrad()       # DO MINIMIZATION <<<<<<<<<<
    #print(m2.errors) # error view
    #print(m2.values) # value view

    print(m2.fmin)   #NICE table
    print(m2.params) # NICE table

    yf = model_chi2( x,
                     m2.values['a'],
                     m2.values['b'],
                     m2.values['c'],
                     m2.values['d']
    )


    print(f"i... Chebyshev parameters are not real! they are for shifted X")
    print("_________________________________________________")
    return yf
