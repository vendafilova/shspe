import sys
sys.path.append('/usr/lib/root/')
from ROOT import gROOT, gStyle, TCanvas, TLegend, kWhite, TGraphErrors
import numpy

def plot_graph(h):
	if h == 'a':
		ix = 0; idx = 1; iy = 2; idy = 3
	else:
		ix = 4; idx = 5; iy = 6; idy = 7
	xs, dxs, ys, dys = numpy.loadtxt('graph_data.dat', usecols=(ix, idx, iy, idy), unpack =1)
	return xs, dxs, ys, dys

gROOT.Reset()
gROOT.SetStyle("Plain")
gStyle.SetOptStat("")
canvas_one = TCanvas("canvas_one","graphs",400, 200, 700, 500)
axs, adxs, ays, adys = plot_graph('a')
print( type(axs))    # numpy.ndarray
print( type(axs[0])) # numpy.float64
print( axs)
graph_a = TGraphErrors(len(axs),axs.flatten("C"),ays.flatten("C"),adxs.flatten("C"),adys.flatten("C"))
bxs, bdxs, bys, bdys = plot_graph('b')
graph_b = TGraphErrors(len(bxs),bxs.flatten("C"),bys.flatten("C"),bdxs.flatten("C"),bdys.flatten("C"))
graph_a.Draw('AC')
graph_b.Draw('same, C')
graph_legend = TLegend(0.14,0.68,0.4,0.88)
graph_legend.SetHeader("Two Graphs")
graph_legend.AddEntry(graph_a,"Graph A","l")
graph_legend.AddEntry(graph_b,"Graph B","l")
graph_legend.SetFillColor(kWhite)
graph_legend.Draw('same')
canvas_one.Update()

#raw_input("Press Enter to Continue: ")
